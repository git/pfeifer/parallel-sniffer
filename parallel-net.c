#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <sched.h>


#include "ev.h"

#define	LISTENING_PORT "6666"
#define	DEFAULT_BACKLOG 32

struct thread_info {
	pthread_t thread_id;
	int thread_num;
	int srv_fd;
};

/* returns the number of online CPU's on the system or 1 as fallback */
static int cpu_num(void)
{
	long num;

	num = sysconf(_SC_NPROCESSORS_ONLN);
	if (num < 1 || num > INT_MAX) {
		fprintf(stderr, "INFO: only 1 online CPU's\n");
		return 1;
	}

	fprintf(stderr, "INFO: %ld online CPU's\n", num);

	return (int)num;
}



/* act as a echo server */
static void process_established_client_cb(int fd, int what, void *data)
{
	int rret, ret;
	char buf[128];

	(void) what;
	(void) data;

	fprintf(stdout, "process established client request\n");

	/* No failure catched here, anyway, the loop exit if EWOULDBLOCK
	 * is active too, so this is all we want */
	while ((rret = read(fd, buf, sizeof(buf))) > 0) {
		ret = write(fd, buf, rret);
		if (ret < 0 && errno == EWOULDBLOCK) {
			/* FIXME: this is up to the reader, we should add
			 * the client fd via EV_WRITE, we should defer
			 * therefore write() and later on when buffer is
			 * available we should resend. Currently this is not
			 * catched here */
			fprintf(stderr, "pipe full, would block\n");
			return;
		}
	}

	if (errno == EPIPE) {
		/* read comment and handle comment - this is demonstration only code */
		fprintf(stderr, "broken pipe, remove event structure from event mechanism\n");
	}

	fprintf(stdout, "process_established_client_cb() return to epoll/select loop\n");
}

static void process_new_client(struct ev *ev, int cli_fd)
{
	int ret;
	struct ev_entry *ev_entry;

	fprintf(stdout, "process new client\n");

	/* try to read data from client first */
	process_established_client_cb(cli_fd, EV_READ, ev);

	/* now we put the cli_fd in the event queue as well */
	ev_entry = ev_entry_new(cli_fd, EV_READ, &process_established_client_cb, ev);
	if (!ev_entry) {
		fprintf(stderr, "failure in creating ev event structure\n");
		return;
	}

	ret = ev_add(ev, ev_entry);
	if (ret != EV_SUCCESS) {
		fprintf(stderr, "Cannot add ev event to event mechanism\n");
		return;
	}
}

static void accept_cb(int fd, int what, void *data)
{
	int cli_fd, ret;
	struct ev *ev;
	struct sockaddr_storage sa;
	socklen_t sa_len = sizeof sa;
	char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

	(void) what;

	ev = data;

	fprintf(stdout, "accept callback called\n");

	cli_fd = accept(fd, (struct sockaddr *) &sa, &sa_len);
	if (cli_fd == -1) {
		if (errno == EWOULDBLOCK) {
			fprintf(stderr, "accept return EWOULDBLOCK\n");
			return;
		}

		fprintf(stderr, "accept() error: %s\n", strerror(errno));
		return;
	}

	ret = ev_set_non_blocking(cli_fd);
	if (ret != EV_SUCCESS) {
		fprintf(stderr, "cannot set non-blocking\n");
		return;
	}

	ret = getnameinfo((struct sockaddr *)&sa, sa_len, hbuf,
			NI_MAXHOST, sbuf, NI_MAXSERV, NI_NUMERICHOST | NI_NUMERICSERV);
	if (ret != 0) {
		fprintf(stderr, "getnameinfo error: %s\n",  gai_strerror(ret));
		return;
	}

	fprintf(stdout, "connection established from %s:%s\n", hbuf, sbuf);

	process_new_client(ev, cli_fd);
}

static void set_affinity(int num)
{
	cpu_set_t mask;

	if (sched_getaffinity(0, sizeof(mask), &mask) < 0) {
		perror("sched_getaffinity");
	}

	CPU_ZERO(&mask);
	CPU_SET(num, &mask);

	if (sched_setaffinity(0, sizeof(mask), &mask) < 0) {
		    perror("sched_setaffinity");
	}

}

/* main entry function for every thread, data is a pointer to the server
 * filedescriptor */
static void *thread_entry(void *data)
{
	int ret, ev_flags;
	struct ev *ev;
	struct ev_entry *ev_entry;
	struct thread_info *tinfo = (struct thread_info *) data;

	set_affinity(tinfo->thread_num - 1);

	fprintf(stderr, "thread %d initialize libeve\n", tinfo->thread_num);
	ev = ev_new();
	if (!ev) {
		fprintf(stderr, "Cannot initialize libev\n");
		return NULL;
	}

	ev_entry = ev_entry_new(tinfo->srv_fd, EV_READ, &accept_cb, ev);
	if (!ev_entry) {
		fprintf(stderr, "failure in creating ev event structure\n");
		return NULL;
	}

	ret = ev_add(ev, ev_entry);
	if (ret != EV_SUCCESS) {
		fprintf(stderr, "Cannot add ev event to event mechanism\n");
		return NULL;
	}

	ev_flags = 0;

	ret = ev_loop(ev, ev_flags);
	if (ret != EV_SUCCESS) {
		fprintf(stderr, "Cannot start event loop - ev_loop()\n");
		return NULL;
	}

	return NULL;
}

/* called to create cpu_num() threads */
static int fire_up_threads(int num, int srv_fd)
{
	int i, ret;
	void *res;
	pthread_attr_t attr;
	struct thread_info *tinfo;

	ret = pthread_attr_init(&attr);
	if (ret != 0) {
		fprintf(stderr, "pthread_attr_init() failed: %s\n", strerror(errno));
		return -1;
	}

	tinfo = calloc(num, sizeof(struct thread_info));
	if (tinfo == NULL) {
		fprintf(stderr, "calloc failed: %s\n", strerror(errno));
		return -1;
	}



	for (i = 0; i < num; i++) {

		tinfo[i].thread_num = i + 1;
		tinfo[i].srv_fd     = srv_fd;

		ret = pthread_create(&tinfo[i].thread_id, &attr,
				&thread_entry, &tinfo[i]);
		if (ret != 0) {
			fprintf(stderr, "pthread_create() failed: %s\n", strerror(errno));
			return -1;
		}
	}

	ret = pthread_attr_destroy(&attr);
	if (ret != 0) {
		fprintf(stderr, "pthread_attr_destroy() failed: %s\n", strerror(errno));
		return -1;
	}

	for (i = 0; i < num; i++) {
		ret = pthread_join(tinfo[i].thread_id, &res);
		if (ret != 0) {
			fprintf(stderr, "pthread_join() failed: %s\n", strerror(errno));
			return -1;
		}

		fprintf(stdout, "joined with thread %d\n", tinfo[i].thread_num);
		free(res);
	}
	free(tinfo);

	return 0;
}


/* open a passive server socket, bind to localhost */
static int open_server_socket(int af_family, const char *port)
{
	int ret, fd = -1, on = 1;
	struct addrinfo hosthints, *hostres, *addrtmp;
	struct protoent *protoent;

	memset(&hosthints, 0, sizeof(struct addrinfo));

	hosthints.ai_family   = af_family;
	hosthints.ai_socktype = SOCK_STREAM;
	hosthints.ai_protocol = IPPROTO_TCP;
	hosthints.ai_flags    = AI_ADDRCONFIG | AI_PASSIVE;

	ret = getaddrinfo(NULL, port, &hosthints, &hostres);
	if (ret != 0) {
		fprintf(stderr, "failure in getaddrinfo: %s\n",
				(ret == EAI_SYSTEM) ?  strerror(errno) : gai_strerror(ret));
		return -1;
	}

	for (addrtmp = hostres; addrtmp != NULL ; addrtmp = addrtmp->ai_next) {
		fd = socket(addrtmp->ai_family, addrtmp->ai_socktype, addrtmp->ai_protocol);
		if (fd < 0)
			continue;

		protoent = getprotobynumber(addrtmp->ai_protocol);
		if (protoent)
			fprintf(stdout, "socket created - protocol %s(%d)\n",
					protoent->p_name, protoent->p_proto);


		ret = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
		if (ret) {
			fprintf(stderr, "failure in setsockopt(): %s\n", strerror(errno));
			return  -1;
		}

		ret = bind(fd, addrtmp->ai_addr, addrtmp->ai_addrlen);
		if (ret) {
			fprintf(stderr, "bind failed: %s\n", strerror(errno));
			close(fd);
			fd = -1;
			continue;
		}

		ret = listen(fd, DEFAULT_BACKLOG);
		if (ret < 0) {
			fprintf(stderr, "listen failed: %s\n", strerror(errno));
			close(fd);
			fd = -1;
			continue;
		}

		/* great, found a valuable socket */
		break;
	}

	if (fd < 0) {
		fprintf(stderr, "Don't found a suitable TCP socket to connect to the client"
				", giving up");
		return -1;
	}

	fprintf(stdout, "bind to port %s via TCP using IPPROTO_TCP socket [%s:%s]\n",
			port,
			addrtmp->ai_family == AF_INET ? "0.0.0.0" : "::", port);

	freeaddrinfo(hostres);

	ret = ev_set_non_blocking(fd);
	if (ret != EV_SUCCESS) {
		fprintf(stderr, "cannot set non-blocking\n");
		return -1;
	}

	return fd;

}

int main(void)
{
	int num_cpu, srv_fd, ret;
	struct sigaction sa = { .sa_handler = SIG_IGN };

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	ret = sigaction(SIGPIPE, &sa, NULL);
	if (ret == -1) {
		fprintf(stderr, "Cannot ignore SIGPIPE: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	num_cpu = cpu_num();

	srv_fd = open_server_socket(AF_UNSPEC, LISTENING_PORT);
	if (srv_fd < 0) {
		fprintf(stderr, "Cannot open server socket\n");
		exit(EXIT_FAILURE);
	}

	/* does not return */
	ret = fire_up_threads(num_cpu, srv_fd);
	if (ret < 0) {
		fprintf(stderr, "Failure in creating threads\n");
		goto err_threads;
	}

	return EXIT_SUCCESS;

err_threads:
	close(srv_fd);

	return EXIT_FAILURE;
}
