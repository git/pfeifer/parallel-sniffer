TARGET = parallel-net
SOURCE_FILES= parallel-net.c ev.c

CC := gcc

CFLAGS    += -pipe -g -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE
CFLAGS    += -ggdb3 -lpthread -lrt
CFLAGS    += -DHAVE_EPOLL
CFLAGS    += -Wall -Wextra -Wwrite-strings -Wsign-compare \
					-Wshadow -Wformat=2 -Wundef -Wstrict-prototypes   \
					-fno-strict-aliasing -fno-common -Wformat-security \
					-Wformat-y2k -Winit-self -Wredundant-decls \
					-Wstrict-aliasing=3 -Wswitch-default -Wswitch-enum \
					-Wno-system-headers -Wundef -Wvolatile-register-var \
					-Wcast-align -Wbad-function-cast -Wwrite-strings \
					-Wold-style-definition  -Wdeclaration-after-statement \
					-fstack-protector

OBJECT_FILES= $(patsubst %.c,%.o,$(SOURCE_FILES))

.SUFFIXES:
.SUFFIXES: .c .o

.PHONY: all clean

all: $(TARGET)

clean:
	@rm -f $(TARGET) $(OBJECT_FILES) core ~*

$(TARGET): $(OBJECT_FILES)
	$(CC) $(CFLAGS) $^ -o $@

cscope:
	cscope -R -b


